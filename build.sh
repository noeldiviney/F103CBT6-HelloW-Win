#!/bin/bash

cmake -S . -B build -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=toolchain-STM32F407.cmake
cmake --build build
