MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     -----------------------------------------------------------------------------")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     PROJECT_NAME                   = ${PROJECT_NAME}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     cmake_minimum_required         = VERSION ${CMAKE_VERSION}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     CMake Version                  = ${CMAKE_VERSION}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     OPERATING System               = ${OPERATING_SYSTEM}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     OS_LC                          = ${OS_LC}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     Using Hard Drive               = ${DRV}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     INSTALL_FOLDER                 = ${INSTALL_FOLDER}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     Arduino Version                = ${ARDUINO_VERSION}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}     BUILD_SYSTEM                  = ${BUILD_SYSTEM}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD_Vendor                   = ${BOARD_VENDOR}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD                          = ${BOARD}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD_VENDOR_VERSION           = ${BOARD_VENDOR_VERSION}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PREFIX                     = ${MCU_PREFIX}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_ARCH                       = ${MCU_ARCH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    STM32_CORE_VERSION             = ${STM32_CORE_VERSION}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    STM32CUBE_VARIANT              = ${STM32CUBE_VARIANT}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    GCC_TOOLCHAIN                  = ${GCC_TOOLCHAIN}") 
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    GCC_VERSION                    = ${GCC_VERSION}") 
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    GCC_SUFFIX                     = ${GCC_SUFFIX}") 
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    TOOLS_PATH                     = ${TOOLS_PATH}") 
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    DEBUG_PROBE                    = ${DEBUG_PROBE}") 
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROTOCOL                       = ${PROTOCOL}") 

MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_SERIES                     = ${MCU_SERIES}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_SERIES_TYPE                = ${MCU_SERIES_TYPE}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_FAMILY                     = ${MCU_FAMILY}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PRODUCT                    = ${MCU_PRODUCT}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PIN_COUNT                  = ${MCU_PIN_COUNT}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_FLASH_SIZE                 = ${MCU_FLASH_SIZE}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_PACKAGE                    = ${MCU_PACKAGE}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_TEMP_RANGE                 = ${MCU_TEMP_RANGE}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BOARD_NAME                     = ${BOARD_NAME}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    SKETCH_NAME                    = ${SKETCH_NAME}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROJECT_NAME                   = ${PROJECT_NAME}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROJECT_NAME_LIST              = ${PROJECT_NAME_LIST}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BUILD_VARIANT_SERIES           = ${BUILD_VARIANT_SERIES}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BUILD_PRODUCT_LINE             = ${BUILD_PRODUCT_LINE}") 

MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    --------------------------------------------")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_ARCH                       = ${MCU_ARCH}") 
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BASE_PATH                      = ${BASE_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    PROJECTS_PATH                  = ${PROJECTS_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    APP_PATH                       = ${APP_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    CORE_PATH                      = ${CORE_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    TOOLS_PATH                     = ${TOOLS_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    TOOLCHAIN_PATH                 = ${TOOLCHAIN_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    CMSIS_PATH                     = ${CMSIS_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    STM32_CUBE_PATH                = ${STM32_CUBE_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    VARIANTS_PATH                  = ${VARIANTS_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    VARIANT_PATH                   = ${VARIANT_PATH}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    MCU_LINKER_SCRIPT              = ${MCU_LINKER_SCRIPT}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    HEX_FILE                       = ${HEX_FILE}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    BIN_FILE                       = ${BIN_FILE}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    OPENOCD_COMMAND                = ${OPENOCD_COMMAND}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}    OPENOCD_CFG                    = ${OPENOCD_CFG}")
MESSAGE(STATUS "CMakeLists.txt        line ${${CCLL}}")

