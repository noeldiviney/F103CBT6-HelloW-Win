# the name of the target operating system
if (_IS_TOOLCHAIN_PROCESSED)
    return()
endif ()
set(_IS_TOOLCHAIN_PROCESSED True)
set(CMAKE_SYSTEM_NAME      Generic)
set(CMAKE_SYSTEM_VERSION   1)
set(CMAKE_SYSTEM_PROCESSOR ARM)

# which compilers to use for C and C++
set(CMAKE_C_COMPILER   ${TOOLCHAIN_PATH}/arm-none-eabi-gcc${GCC_SUFFIX}      CACHE PATH "" FORCE)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PATH}/arm-none-eabi-g++${GCC_SUFFIX}      CACHE PATH "" FORCE)
set(CMAKE_ASM_COMPILER ${TOOLCHAIN_PATH}/arm-none-eabi-gcc${GCC_SUFFIX}      CACHE PATH "" FORCE)
set(CMAKE_OBJCOPY      ${TOOLCHAIN_PATH}/arm-none-eabi-objcopy${GCC_SUFFIX}  CACHE PATH "" FORCE)
set(CMAKE_OBJDUMP      ${TOOLCHAIN_PATH}/arm-none-eabi-objdump${GCC_SUFFIX}  CACHE PATH "" FORCE)
set(CMAKE_AR           ${TOOLCHAIN_PATH}/arm-none-eabi-ar${GCC_SUFFIX}       CACHE PATH "" FORCE)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# core flags
if(${MCU_SERIES_TYPE} STREQUAL F4xx)
	set(CORTEX_FLAGS "-mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard")
elseif(${MCU_SERIES_TYPE} STREQUAL F1xx)
	set(CORTEX_FLAGS "-mcpu=cortex-m3 -mfloat-abi=soft")
else()
	MESSAGE(FATAL "Stm32 Family Series not set ...   see STM32Toolchain.cmake line 35")
endif()

set(ARM_OPTIONS -mcpu=cortex-m3 -mthumb --specs=nano.specs)
add_compile_options(
  ${ARM_OPTIONS}
  -fmessage-length=0
  -funsigned-char
  -ffunction-sections
  -fdata-sections
  -MMD
  -MP)

add_compile_definitions(
  ${BUILD_VARIANT_SERIES}
  ARDUINO=${ARDUINO_VERSION2}
  ARDUINO_${MCU_PRODUCT}_${BOARD_NAME}
  ARDUINO_ARCH_STM32
  BOARD_NAME=${BOARD_NAME}
  "VARIANT_H=\"variant_${MCU_PRODUCT}.h\" "
  ${BUILD_PRODUCT_LINE}
  HAL_UART_MODULE_ENABLED
  USE_FULL_LL_DRIVER
#  USE_FULL_ASSERT
#  OS_USE_TRACE_SEMIHOSTING_STDOUT
#  OS_USE_SEMIHOSTING
)

add_link_options(
  ${ARM_OPTIONS}
#  -fno-use-cxa-atexit
  --specs=nano.specs
  -specs=nosys.specs
  -Wl,--defsym=LD_FLASH_OFFSET=0
  -Wl,--defsym=LD_MAX_SIZE=131072
  -Wl,--defsym=LD_MAX_DATA_SIZE=20480
  -Wl,--cref
  -Wl,--check-sections
  -Wl,--gc-sections
  -Wl,--entry=Reset_Handler
  -Wl,--unresolved-symbols=report-all
  -Wl,--warn-common
  -Wl,--default-script=${MCU_LINKER_SCRIPT}
  -Wl,-Map,${PROJECTS_PATH}/build/${PROJECT_NAME}.map
  -larm_cortexM3l_math
  -L${BASE_PATH}/tools/CMSIS/CMSIS/DSP/Lib/GCC/
  -lc
  -lm
  -lgcc
  -lstdc++   
#  -nostartfiles
)

##===========================================================================
if(FALSE) # fake a block comment
##===========================================================================

# compiler: language specific flags
set(CORE_FLAGS "${CORTEX_FLAGS} -mthumb -mthumb-interwork -mlittle-endian --specs=nano.specs --specs=nosys.specs")

set(CMAKE_C_FLAGS "${CORE_FLAGS} -fno-builtin -Wall -std=gnu99 -fdata-sections -ffunction-sections -g3 -gdwarf-2" CACHE INTERNAL "c compiler flags")
set(CMAKE_C_FLAGS_DEBUG        "" CACHE INTERNAL "c compiler flags: Debug")
set(CMAKE_C_FLAGS_RELEASE      "" CACHE INTERNAL "c compiler flags: Release")

set(CMAKE_CXX_FLAGS            "${CORE_FLAGS} -fno-rtti -fno-exceptions -fno-builtin -Wall -std=gnu++11 -fdata-sections -ffunction-sections -g -ggdb3" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_CXX_FLAGS_DEBUG      "" CACHE INTERNAL "cxx compiler flags: Debug")
set(CMAKE_CXX_FLAGS_RELEASE    "" CACHE INTERNAL "cxx compiler flags: Release")

set(CMAKE_ASM_FLAGS            "${CORE_FLAGS} -g -ggdb3 -D__USES_CXX" CACHE INTERNAL "asm compiler flags")
set(CMAKE_ASM_FLAGS_DEBUG      "" CACHE INTERNAL "asm compiler flags: Debug")
set(CMAKE_ASM_FLAGS_RELEASE    "" CACHE INTERNAL "asm compiler flags: Release")
#return()
##===========================================================================
endif() # fake a block comment
##===========================================================================

# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# find additional toolchain executables
find_program(ARM_SIZE_EXECUTABLE arm-none-eabi-size)
find_program(ARM_GDB_EXECUTABLE arm-none-eabi-gdb)
find_program(ARM_OBJCOPY_EXECUTABLE arm-none-eabi-objcopy)
find_program(ARM_OBJDUMP_EXECUTABLE arm-none-eabi-objdump)

