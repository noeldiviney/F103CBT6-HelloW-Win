#!"C:\Program Files\PowerShell\7\pwsh.exe" -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------

#param([string]$OS           = "WIN",
#      [string]$DRIVE        = "DRIVE",
#      [string]$SLASH        = "SLASH");

#$SLASH                      = ""
#$BASE_ROOT                  = ""


#	if(${OS} -eq "WIN")
#    { 
#        ${SLASH} = "\";
#	    ${BASE_ROOT} = "${DRIVE}:${SLASH}"	
#    }

$OS                         = "WIN"
$DRIVE                      = "C"
$SLASH                      = "\"
$BASE_PATH                  = "${DRIVE}:${SLASH}"
$INSTALL_FOLDER             = "DinRDuino"
$PROJECT_PATH               = "${BASE_PATH}${INSTALL_FOLDER}${SLASH}Projects"
$ARDUINO_VERSION            = "1.8.18"
$ARDUINO_LIBRARIES_RELEASE  = "1.0.1"
$ARDUINO_CLI_RELEASE        = "0.20.2"
$BOARD_VENDOR_EI            = "Eicon"
$BOARD_VENDOR_ST            = "STMicroelectronics"
$SKETCHBOOK_RELEASE         = "0.1.0"
$OPENOCD_RELEASE            = "0.11.0-1"
$OPENOCD_CFG_RELEASE        = "0.1.0"
$ARM_GCC_VERSION                = "10.1.0"
$EICON_GCC_VERSION          = "5.4.1"
$CMSIS_VERSION              = "5.7.0"

$OPENOCD_TB_TYPE            = "win32-x64.zip"
$ARDUINO_LIBRARIES_PREFIX   = "eicon_win_arduino_libraries"
$SKETCHBOOK_NAME_PREFIX     = "eicon_win_sketchbook"
$XPACK_GCC_NAME             = "xpack-arm-none-eabi-gcc"
$EICON_GCC_NAME             = "eicon-arm-none-eabi-gcc"
$ARDUINO_CLI_TB_TYPE        = "Windows_64bit.zip"
$ARDUINO_CLI_TB_NAME        = "arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$ARDUINO_TB_TYPE            = "windows.zip"
$OPENOCD_CFG_SCRIPT_NAME    = "eicon_win_openocd_cfg"
$ARDUINO_PATH               = "${BASE_PATH}${SLASH}arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH              = "${ARDUINO_PATH}${SLASH}portable"
$HARDWARE_PATH_EI           = "${PORTABLE_PATH}${SLASH}packages${SLASH}${BOARD_VENDOR_EI}${SLASH}hardware"
$HARDWARE_PATH_ST           = "${PORTABLE_PATH}${SLASH}packages${SLASH}${BOARD_VENDOR_ST}${SLASH}hardware"
$TOOLS_PATH_EI              = "${PORTABLE_PATH}${SLASH}packages${SLASH}${BOARD_VENDOR_EI}${SLASH}tools"
$TOOLS_PATH_ST              = "${PORTABLE_PATH}${SLASH}packages${SLASH}${BOARD_VENDOR_ST}${SLASH}tools"
$OPENOCD_PATH               = "${BASE_PATH}${SLASH}Openocd"
$DLOAD_PATH                 = "${BASE_PATH}${SLASH}dload"
$PREFS_PATH                 = "${PORTABLE_PATH}"
$SKETCH_PATH                = "${PORTABLE_PATH}${SLASH}sketchbook${SLASH}arduino"

$ARDUINO_CLI_URI            = "https://downloads.arduino.cc/arduino-cli/arduino-cli_${ARDUINO_CLI_RELEASE}_${ARDUINO_CLI_TB_TYPE}"
$OPENOCD_CFG_SCRIPT_URI     = "https://gitlab.com/noeldiviney/${OPENOCD_CFG_SCRIPT_NAME}/-/archive/${OPENOCD_CFG_RELEASE}/${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}.tar.gz"
$ARDUINO_LIBRARIES_ZIP_URI  = "https://gitlab.com/noeldiviney/eicon_win_arduino_libraries/-/archive/${ARDUINO_LIBRARIES_RELEASE}/${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}.zip"
$ARDUINO_ZIP_URI            = "https://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-${ARDUINO_TB_TYPE}"
$SKETCHBOOK_ZIP_URI         = "https://gitlab.com/noeldiviney/eicon_win_sketchbook/-/archive/${SKETCHBOOK_RELEASE}/eicon_win_sketchbook-${SKETCHBOOK_RELEASE}.zip"
$OPENOCD_TB_NAME            = "xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$OPENOCD_TB_URI             = "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v${OPENOCD_RELEASE}/xpack-openocd-${OPENOCD_RELEASE}-${OPENOCD_TB_TYPE}"
$STM32_INDEX                = "        https://gitlab.com/noeldiviney/eicon_win_packages/-/raw/main/package_eicon_stm32_index.json,"
$KINETIS_INDEX              = "        https://gitlab.com/noeldiviney/eicon_win_packages/-/raw/main/package_eicon_kinetis_index.json,"
$STMicroelectronics_INDEX   = "        https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json"

$ARDUINO_ZIP_FILE           = "arduino-${ARDUINO_VERSION}.zip"
$ARDUINO_LIBRARIES_ZIP_FILE = "eicon_win_arduino_libraries-${ARDUINO_LIBRARIES_RELEASE}.zip"
$SKETCH_NAME                = "${VENDOR}-${BOARD}-${CPU}-${SKETCH}-${SKETCH_VER}"
$USER                       = "$env:UserName"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH               = "/home/$env:USER"
$SCRIPT_PATH                = "${BASE_ROOT}bin"
$OPENOCD_ZIP_FILE           = "eicon-openocd-${OPENOCD_RELEASE}.zip"
$ARDUINO_CLI_ZIP_FILE       = "arduino_cli.zip"
$SKETCHBOOK_ZIP_FILE        = "sketchbook.zip"
$SourceFileLocation         = "${BASE_ROOT}Program Files${SLASH}Notepad++${SLASH}notepad++.exe"

#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------";
    Write-Host "Line $(CurrentLine)  Entering                     main"; 

#    Write-Host "Line $(CurrentLine)  Calling                      setup_os_stuff";
#    setup_os_stuff
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"


    Write-Host "Line $(CurrentLine)  Calling                      create_${BASE_PATH}";
    create_${BASE_PATH}
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      install_arduino-${VERSION}";
    install_arduino-${VERSION}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      install_arduino-libraries";
    install_arduino-libraries
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_openocd-${OPENOCD_RELEASE}"
    install_openocd-${OPENOCD_RELEASE}
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_openocd_cfg_scripts"
    install_openocd_cfg_scripts
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cli"
    install_arduino_cli
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_arduino_cli_config"
    install_arduino_cli_config
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      core_update-index"
    core_update-index
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    
    Write-Host "Line $(CurrentLine)  Calling                      core_install_stm32"
    core_install_stm32
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      core_install_kinetis"
    core_install_kinetis
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      core_install_stmicro"
    core_install_stmicro
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      reconfig_paths"
    reconfig_paths                                                                               # Disable for further testing
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      install_sketchbook"
    install_sketchbook
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

#    Write-Host "Line $(CurrentLine)  Calling                      install_DesktopShortcut"       # Work in Profress !!!
#    install_DesktopShortcut

#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
}

#---------------------------------------------------------
# install_DesktopShortcut
#---------------------------------------------------------
function install_DesktopShortcut
{
#$WScriptShell = New-Object -ComObject WScript.Shell
    Write-Host "Line $(CurrentLine)  Entering                     install_DesktopShortcut";
#    $SourceFileLocation = "C:${SLASH}Program Files${SLASH}PowerShell${SLASH}7${SLASH}pwsh.exe"
#    $ShortcutLocation = "C:${SLASH}Users${SLASH}$USER${SLASH}Desktop${SLASH}pwsh.exe.lnk"
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    $Shortcut = "$WScriptShell.CreateShortcut($ShortcutLocation)" 
#    $Shortcut.TargetPath = $SourceFileLocation "C:${SLASH}Program Files${SLASH}PwrShell${SLASH}arduino-install.ps1" "W Dinrduino5 1.8.15 Eicon"
#    $Shortcut.Save()
#    Write-Host "Line $(CurrentLine)  Executing                 $ShortcutLocation = "C:${SLASH}Users${SLASH}$USER${SLASH}Desktop${SLASH}Notepad++.lnk";
#    $ShortcutLocation = "C:${SLASH}Users${SLASH}$USER${SLASH}Desktop${SLASH}Notepad++.lnk"
#    Write-Host "Line $(CurrentLine)  Executing                 $WScriptShell = New-Object -ComObject WScript.Shell";                 
#    $WScriptShell = New-Object -ComObject WScript.Shell
#    Write-Host "Line $(CurrentLine)  Executing                 '$Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)'";
#    $Shortcut = $WScriptShell.CreateShortcut($ShortcutLocation)
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.TargetPath = $SourceFileLocation";
#    $Shortcut.TargetPath = $SourceFileLocation
#    Write-Host "Line $(CurrentLine)  Executing                 $Shortcut.Save()";
#    $Shortcut.Save()
    Write-Host "Line $(CurrentLine)  Leaving                      install_DesktopShortcut";
}
#---------------------------------------------------------
# install_sketchbook
#---------------------------------------------------------
function install_sketchbook
{
    Write-Host "Line $(CurrentLine)  Entering                     install_sketchbook";
    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE}";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE}";
    Invoke-WebRequest -Uri ${SKETCHBOOK_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE}
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PORTABLE_PATH}${SLASH}sketchbook)";
    if (Test-Path ${PORTABLE_PATH}${SLASH}sketchbook)
    {
        Write-Host "Line $(CurrentLine)  sketchbook folder            Exists , Deleting";
        Remove-Item -Recurse -Force ${PORTABLE_PATH}${SLASH}sketchbook
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}${SLASH}";
    Expand-Archive -Force -PATH ${BASE_PATH}${SLASH}dload${SLASH}${SKETCHBOOK_ZIP_FILE} -Destination ${PORTABLE_PATH}${SLASH} 
    Write-Host "Line $(CurrentLine)  Renaming                     ${PORTABLE_PATH}${SLASH}${SKETCHBOOK_NAME_PREFIX}*";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}${SLASH}sketchbook";
    Rename-Item ${PORTABLE_PATH}${SLASH}${SKETCHBOOK_NAME_PREFIX}* ${PORTABLE_PATH}${SLASH}sketchbook
    Write-Host "Line $(CurrentLine)  Leaving                      install_sketchbook";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
#  reconfig_paths
#---------------------------------------------------------
function reconfig_paths
{
    Write-Host "Line $(CurrentLine)  Entering                     reconfig_paths";
    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH}*${SLASH} -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH} ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH}*${SLASH} -Recurse | Move-Item -Force -Destination ${TOOLS_PATH_EI}${SLASH}CMSIS${SLASH}

    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH} ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item  -Force -Destination ${TOOLS_PATH_EI}${SLASH}${XPACK_GCC_NAME}${SLASH}

    Write-Host "Line $(CurrentLine)  Exexcuting                   Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item  -Destination ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH} ";
    Get-ChildItem -Path ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH}*${SLASH} -Recurse | Move-Item -Force -Destination ${TOOLS_PATH_EI}${SLASH}${EICON_GCC_NAME}${SLASH}
    Write-Host "Line $(CurrentLine)  Leaving                      reconfig_paths";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_stmicro
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stmicro";
    if (Test-Path ${ST_HARDWARE_PATH}${SLASH}stm32)
    {
        Write-Host "Line $(CurrentLine)  stmicro_core                 Exists , Deleting";
        Remove-Item -Recurse -Force ${ST_HARDWARE_PATH}${SLASH}stm32
        Remove-Item -Recurse -Force ${TOOLS_PATH}${SLASH}${EICON_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_ST}:stm32";
    & $PORTABLE_PATH${SLASH}arduino-cli core install ${BOARD_VENDOR_ST}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_micro";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_kinetis
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_kinetis";
    if (Test-Path ${EI_HARDWARE_PATH}${SLASH}kinetis)
    {
        Write-Host "Line $(CurrentLine)  kinetis_core                 Exists , Deleting";
        Remove-Item -Recurse -Force ${EI_HARDWARE_PATH}${SLASH}kinetis
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH}${SLASH}${EICON_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_EI}:kinetis";
    & $PORTABLE_PATH${SLASH}arduino-cli core install ${BOARD_VENDOR_EI}:kinetis    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_kinetis";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

function core_install_stm32
{
    Write-Host "Line $(CurrentLine)  Entering                     core_install_stm32";
    if (Test-Path ${EI_HARDWARE_PATH}${SLASH}stm32)
    {
        Write-Host "Line $(CurrentLine)  stm32_core                   Exists , Deleting";
        Remove-Item -Recurse -Force ${EI_HARDWARE_PATH}${SLASH}stm32
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH}${SLASH}CMSIS
        Remove-Item -Recurse -Force ${EI_TOOLS_PATH}${SLASH}${XPACK_GCC_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core install ${BOARD_VENDOR_EI}:stm32";
    & $PORTABLE_PATH${SLASH}arduino-cli core install ${BOARD_VENDOR_EI}:stm32    
    Write-Host "Line $(CurrentLine)  Leaving                      core_install_stm32";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# core_update-index
#---------------------------------------------------------
function core_update-index
{
    Write-Host "Line $(CurrentLine)  Entering                     core_update-index";
    Write-Host "Line $(CurrentLine)  Executing                    cd ${PORTABLE_PATH}";
    cd ${PORTABLE_PATH}
    Write-Host "Line $(CurrentLine)  Current Working Dir        = $PWD";
    Write-Host "Line $(CurrentLine)  Executing                    arduino-cli core update-index";
    & $PORTABLE_PATH${SLASH}arduino-cli core update-index    
    Write-Host "Line $(CurrentLine)  Leaving                      core_update-index";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_arduino_cli_config
#---------------------------------------------------------
function install_arduino_cli_config
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli_config";

    if (Test-Path ${PORTABLE_PATH}${SLASH}arduino-cli.yaml)
    {
        Write-Host "Line $(CurrentLine)  arduino-cli.yaml             Exists , Deleting";
        Remove-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml";
        New-Item ${PORTABLE_PATH}${SLASH}arduino-cli.yaml
    }    
    Write-Host "Line $(CurrentLine)  Adding                       arduino-cli.yaml Contents";
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'board_manager:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    additional_urls: ['
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml          ${STM32_INDEX}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml          ${KINETIS_INDEX}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml          ${STMicroelectronics_INDEX}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    ]'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'daemon:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    port: "50051"'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'directories:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    data: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml             ${PORTABLE_PATH}
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    downloads: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml             ${PORTABLE_PATH}${SLASH}staging
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    user: ' -NoNewLine
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml             ${PORTABLE_PATH}${SLASH}Workspace
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml 'logging:'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    file: ""'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    format: text'
    Add-Content ${PORTABLE_PATH}${SLASH}arduino-cli.yaml '    level: info'
    Write-Host "Line $(CurrentLine)  Editing                      arduino-cli.yaml Completed";
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli_config";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# install_arduino_cli
#---------------------------------------------------------
function install_arduino_cli
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino_cli";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${PORTABLE_PATH})";
    if (Test-Path ${PORTABLE_PATH})
    {
        Write-Host "Line $(CurrentLine)  portable folder              Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Executing                    md ${PORTABLE_PATH}";
        md ${PORTABLE_PATH}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME})
    {
        Write-Host "Line $(CurrentLine)  arduino-cli                  Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME})";
        Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME}";
        Invoke-WebRequest -Uri ${ARDUINO_CLI_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME} -C ${PORTABLE_PATH}";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_CLI_TB_NAME} -C ${PORTABLE_PATH}

    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino_cli";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_openocd_cfg_scripts
#---------------------------------------------------------
function install_openocd_cfg_scripts
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd_cfg_scripts";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME})
    {
        Write-Host "Line $(CurrentLine)  openocd_cfg_scripts          Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_TB_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_CFG_SCRIPT_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}'";
        Invoke-WebRequest -Uri ${OPENOCD_CFG_SCRIPT_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}
    }

    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME} -C ${BASE_PATH}${SLASH}dload${SLASH}";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME} -C ${BASE_PATH}${SLASH}dload${SLASH}
    Write-Host "Line $(CurrentLine)  Executing                    Copy-Item -Recurse -Force -path: "${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}${SLASH}*" -destination: ${BASE_PATH}${SLASH}openocd${SLASH}";
    Copy-Item -Recurse -Force -path: "${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_CFG_SCRIPT_NAME}-${OPENOCD_CFG_RELEASE}${SLASH}*" -destination: ${BASE_PATH}${SLASH}openocd${SLASH}


    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd_cfg_scripts";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_openocd-${OPENOCD_RELEASE}
#---------------------------------------------------------
function install_openocd-${OPENOCD_RELEASE}
{
    Write-Host "Line $(CurrentLine)  Entering                     install_openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME})
    {
        Write-Host "Line $(CurrentLine)  xpack-openocd                Exists";   
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME}";
        Write-Host "Line $(CurrentLine)  Executing                    'Invoke-WebRequest -Uri ${OPENOCD_TB_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME}'";
        Invoke-WebRequest -Uri ${OPENOCD_TB_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME}
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}openocd)";
    if (Test-Path ${BASE_PATH}${SLASH}openocd)
    {
        Write-Host "Line $(CurrentLine)  openocd                      is installed  removing it";
        Write-Host "Line $(CurrentLine)  Executing                    Remove-item -Recurse -Force ${BASE_PATH}${SLASH}openocd ";
        Remove-Item -Recurse -Force ${BASE_PATH}${SLASH}openocd
    }

    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME} -C ${BASE_PATH}${SLASH}dload";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${OPENOCD_TB_NAME} -C ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Renaming                     ${BASE_PATH}${SLASH}xpack-openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${BASE_PATH}${SLASH}openocd";
    Rename-Item  ${BASE_PATH}${SLASH}xpack-openocd-${OPENOCD_RELEASE} ${BASE_PATH}${SLASH}openocd

    Write-Host "Line $(CurrentLine)  Leaving                      install_openocd-${OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# install_arduino-libraries
#---------------------------------------------------------
function install_arduino-libraries
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino-libraries";

    Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}ARDUINO_LIBRARIES";
    Write-Host "Line $(CurrentLine)  Executing                    Invoke-WebRequest -Uri ${ARDUINO_LIBRARIES_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE}";
    Invoke-WebRequest -Uri ${ARDUINO_LIBRARIES_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE}
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${$ARDUINO_PATH}${SLASH}libraries)";
    if (Test-Path ${ARDUINO_PATH}${SLASH}libraries)
    {
        Write-Host "Line $(CurrentLine)  libraries folder            Exists , Deleting";
        Remove-Item -Recurse -Force ${ARDUINO_PATH}${SLASH}libraries
    }	
    Write-Host "Line $(CurrentLine)  Executing                    Expand-Archive -PATH ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE} -Destination ${ARDUINO_PATH}${SLASH}";
    Expand-Archive -Force -PATH ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_LIBRARIES_ZIP_FILE} -Destination ${ARDUINO_PATH}${SLASH} 
    Write-Host "Line $(CurrentLine)  Renaming                     ${ARDUINO_PATH}${SLASH}${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE}";
    Write-Host "Line $(CurrentLine)  To                           ${PORTABLE_PATH}${SLASH}libraries";
    Rename-Item ${ARDUINO_PATH}${SLASH}${ARDUINO_LIBRARIES_PREFIX}-${ARDUINO_LIBRARIES_RELEASE} ${ARDUINO_PATH}${SLASH}libraries
 
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino-libraries";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}


#---------------------------------------------------------
# install_arduino-${VERSION}
#---------------------------------------------------------
function install_arduino-${VERSION}
{
    Write-Host "Line $(CurrentLine)  Entering                     install_arduino-${VERSION}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_ZIP_FILE})";
    if (Test-Path ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_ZIP_FILE})
    {
        Write-Host "Line $(CurrentLine)  arduino-${ARDUINO_VERSION}.zip           Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Downloading                  ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_ZIP_FILE}";
        Invoke-WebRequest -Uri ${ARDUINO_ZIP_URI} -Outfile ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_ZIP_FILE}
    }
    Write-Host "Line $(CurrentLine)  Executing                    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_ZIP_FILE} -C ${BASE_PATH}${SLASH}dload";
    tar -xf ${BASE_PATH}${SLASH}dload${SLASH}${ARDUINO_ZIP_FILE} -C ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Leaving                      install_arduino-${VERSION}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# create_${BASE_PATH}
#---------------------------------------------------------
function create_${BASE_PATH}
{
    Write-Host "Line $(CurrentLine)  Entering                     create_${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH})";
    if (Test-Path ${BASE_PATH})
    {
        Write-Host "Line $(CurrentLine)  Folder                       ${BASE_PATH}  Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${BASE_PATH}";
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
        md ${BASE_PATH}
        Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------";
    }
    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${BASE_PATH})${SLASH}dload";
    if (Test-Path ${BASE_PATH}${SLASH}dload)
    {
        Write-Host "Line $(CurrentLine)  Folder                       ${BASE_PATH}${SLASH}dload  Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${BASE_PATH}${SLASH}dload";
        Write-Host "Line $(CurrentLine)  Executing                    md ${BASE_PATH}${SLASH}dload";
        md ${BASE_PATH}${SLASH}dload
    }
    Write-Host "Line $(CurrentLine)  Executing                    cd ${BASE_PATH}";
    cd ${BASE_PATH}
    Write-Host "Line $(CurrentLine)  Current Directory          = $PWD";
    Write-Host "Line $(CurrentLine)  Leaving                      create_${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host "Line $(CurrentLine)  Operating System           = ${OS}";
    Write-Host "Line $(CurrentLine)  DRIVE                      = ${DRIVE}";
    Write-Host "Line $(CurrentLine)  SLASH                      = ${SLASH}";
    Write-Host "Line $(CurrentLine)  BASE_PATH                  = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER             = ${INSTALL_FOLDER}";	
    Write-Host "Line $(CurrentLine)  PROJECT_PATH               = ${PROJECT_PATH}";	
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION            = ${ARDUINO_VERSION}";
    Write-Host "Line $(CurrentLine)  ARDUINO_LIBRARIES_RELEASE  = $ARDUINO_LIBRARIES_RELEASE";	
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_RELEASE        = ${ARDUINO_CLI_RELEASE}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_EI            = ${BOARD_VENDOR_EI}";            
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_ST            = ${BOARD_VENDOR_ST}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_RELEASE         = ${SKETCHBOOK_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_RELEASE            = ${OPENOCD_RELEASE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME    = ${OPENOCD_CFG_SCRIPT_NAME}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_RELEASE        = ${OPENOCD_CFG_RELEASE}";            
    Write-Host "Line $(CurrentLine)  ARM_GCC_VERSION            = ${ARM_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_VERSION          = ${EICON_GCC_VERSION}";            
    Write-Host "Line $(CurrentLine)  CMSIS_VERSION              = ${CMSIS_VERSION}";            
    Write-Host "Line $(CurrentLine)  PACKAGES_RELEASE           = ${PACKAGES_RELEASE}";            
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  BASE_PATH                  = ${BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH               = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PORTABLE_PATH              = ${PORTABLE_PATH}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_EI           = ${HARDWARE_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  HARDWARE_PATH_ST           = ${HARDWARE_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_EI              = ${TOOLS_PATH_EI}";            
    Write-Host "Line $(CurrentLine)  TOOLS_PATH_ST              = ${TOOLS_PATH_ST}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH                 = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH                = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)";
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_URI            = ${ARDUINO_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_URI             = ${OPENOCD_TB_URI}";
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_URI     = ${OPENOCD_CFG_SCRIPT_URI}";            
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_URI         = ${SKETCHBOOK_ZIP_URI}";            
    Write-Host "Line $(CurrentLine)";	
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_TYPE            = ${OPENOCD_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  XPACK_GCC_NAME             = ${XPACK_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  EICON_GCC_NAME             = ${EICON_GCC_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_TYPE        = ${ARDUINO_CLI_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_CLI_TB_NAME        = ${ARDUINO_CLI_TB_NAME}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_TB_TYPE            = ${ARDUINO_TB_TYPE}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_ZIP_FILE           = ${ARDUINO_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_TB_NANE            = ${OPENOCD_TB_NAME}";                    
    Write-Host "Line $(CurrentLine)  OPENOCD_CFG_SCRIPT_NAME    = ${OPENOCD_CFG_SCRIPT_NAME}";
    Write-Host "Line $(CurrentLine)  SKETCHBOOK_ZIP_FILE        = ${SKETCHBOOK_ZIP_FILE}";            
    Write-Host "Line $(CurrentLine)  USER                       = ${USER}";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}

#---------------------------------------------------------
# Setup OS Stuff
#---------------------------------------------------------
function setup_os_stuff
{
    Write-Host "Line $(CurrentLine)  Entering                     setup_os_stuff"
	if(${OS} -eq "WIN")
    { 
        Write-Host "Line $(CurrentLine)  OS                         = Windows";            
        Write-Host "Line $(CurrentLine)  SLASH                      = ${SLASH}";
        Write-Host "Line $(CurrentLine)  ARDUINO_VERSION            = ${ARDUINO_VERSION}";
        Write-Host "Line $(CurrentLine)  SLASH                      = ${SLASH}";
        ${SLASH} = "\";
        Write-Host "Line $(CurrentLine)  SLASH                      = ${SLASH}";
        ${BASE_ROOT} = "${DRIVE}:${SLASH}"	
        Write-Host "Line $(CurrentLine)  BASE_ROOT                  = ${BASE_ROOT}";
    }
    elseIf(${OS} -eq "LIN")
    {
        Write-Host "Line $(CurrentLine)  OS                         = Linux";            
    }
    ElseIf(${OS} -eq "WSL" )
    {
        Write-Host "Line $(CurrentLine)  OS                         = WSL";            
    }
    Else
    {
        Write-Host "Line $(CurrentLine)  OS                         = Not Defined";            
    }
    Write-Host "Line $(CurrentLine)  Leaving                      setup_os_stuff"
}

#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering                  Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing                 $ARDUINO_PATH${SLASH}arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH${SLASH}arduino
    Write-Host "Line $(CurrentLine)  Leaving                   Launch_Arduino_IDE"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

